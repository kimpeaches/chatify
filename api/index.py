from fastapi import FastAPI
from pymongo import MongoClient
from bson import json_util
import json

from pydantic import BaseModel

class Message(BaseModel):
    username: str
    text: str



app = FastAPI()
client = MongoClient("mongodb+srv://nbytes:Rdhmlwaunnty4jNe@nerdybytes.ap1eoen.mongodb.net/?retryWrites=true&w=majority")
db = client["nerdybytesdb"]

@app.get("/api/python")
def hello_world():
    return {"message": "Hello World"}


@app.get("/api/get-messages")
def get_messages():
    messages = db["messages"]
    data = []
    for document in messages.find({}):
        data.append(document)
    return {"data": json.loads(json_util.dumps(data))}

@app.get("/api/get-users")
def get_users():
    users = db["users"]
    data = []
    for document in users.find({}):
        data.append(document)
    return {"data": json.loads(json_util.dumps(data))}

@app.post("/api/create-message")
def create_message(data: Message):
    messages = db["messages"]
    messages.insert_one(data)
    return {"data": "success"}
