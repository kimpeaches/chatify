import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyD1uVy3fRS78HGwE2-9XTntIGRq7ZdvQ6g",
    authDomain: "chatify-db-78edd.firebaseapp.com",
    projectId: "chatify-db-78edd",
    storageBucket: "chatify-db-78edd.appspot.com",
    messagingSenderId: "496508962104",
    appId: "1:496508962104:web:dbf636532a76e9ba27edcd",
    measurementId: "G-WP11BF49B4",
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const db = getFirestore();
