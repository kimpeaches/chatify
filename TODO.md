Issues to fix:

-   having to --force install packages because mui and next have conflicting dependency versions of react. make sure there are not multiple versions of react

-   keep keyboard open after sending message (autofocus or element.focus()?)
-   make navbar fixed
