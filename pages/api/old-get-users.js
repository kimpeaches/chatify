import { db } from "../../utils/firestore";
import { collection, getDocs } from "firebase/firestore";

export default async function handler(req, res) {
    const data = [];
    const querySnapshot = await getDocs(collection(db, "users"));

    querySnapshot.forEach((doc) => {
        data.push(doc.data());
    });

    res.status(200).json({ data });
}
