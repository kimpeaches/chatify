import { db } from "../../utils/firestore";
import { collection, addDoc } from "firebase/firestore";

export default async function handler(req, res) {
    const docRef = await addDoc(collection(db, "messages"), {
        text: req.body.text,
        createdAt: Date.now(),
        username: req.body.username,
    });

    res.status(200).json({ data: docRef });
}
