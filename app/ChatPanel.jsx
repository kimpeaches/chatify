"use client";

import SendIcon from "@mui/icons-material/Send";
import Divider from "@mui/material/Divider";
import Fab from "@mui/material/Fab";
import Grid from "@mui/material/Grid";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import TextField from "@mui/material/TextField";
import { format } from "date-fns";
import { default as React, useState, useEffect, useRef } from "react";

export default function ChatPanel({ setIsLoading }) {
    const [messages, setMessages] = useState([]);
    const lastItem = useRef(0);

    const fetchData = async () => {
        setIsLoading(true);
        const res = await fetch("/api/get-messages");
        const response = await res.json();
        setMessages(response.data);
        setIsLoading(false);
    };

    const submitMessage = async (e) => {
        e.preventDefault();
        const data = {};
        new FormData(e.target).forEach((value, key) => (data[key] = value));
        const url = "/api/create-message/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };

        setMessages([...messages, data]);
        setIsLoading(true);
        const response = await fetch(url, fetchConfig);
        setIsLoading(false);
        if (response.ok) {
            e.target.reset();
            fetchData();
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    if (lastItem && lastItem.scrollIntoView) {
        lastItem.scrollIntoView({ behavior: "smooth" });
    }

    // const currentUsername = "bigj";
    const currentUsername = "kimpeaches";
    const checkCurrentUser = (username) => username === currentUsername;

    return (
        <>
            <List className="c-message-area">
                <ListItem
                    ref={lastItem}
                    style={{
                        height: 1,
                        padding: 0,
                    }}
                />
                {messages
                    .sort((a, b) => {
                        if (!a.createdAt?.$timestamp?.i) {
                            return -1;
                        } else if (!b.createdAt?.$timestamp?.i) {
                            return 1;
                        } else {
                            return a.createdAt?.$timestamp?.i <
                                b.createdAt?.$timestamp?.i
                                ? 1
                                : -1;
                        }
                    })
                    .map((message) => (
                        <ListItem
                            key={
                                message.username +
                                message.createdAt?.$timestamp?.i
                            }
                            style={{ marginBottom: 15 }}
                        >
                            <Grid container>
                                <Grid
                                    item
                                    xs={12}
                                    align={
                                        checkCurrentUser(message.username)
                                            ? "right"
                                            : "left"
                                    }
                                >
                                    <ListItemText
                                        primary={message.text}
                                        className={
                                            checkCurrentUser(message.username)
                                                ? "c-user-current"
                                                : "c-user-other"
                                        }
                                        style={{
                                            display: "inline-block",
                                            padding: "8px 12px",
                                            borderRadius: "10px",
                                        }}
                                    ></ListItemText>
                                </Grid>
                                <Grid item xs={12}>
                                    <ListItemText
                                        align={
                                            checkCurrentUser(message.username)
                                                ? "right"
                                                : "left"
                                        }
                                        style={{
                                            marginTop: 0,
                                        }}
                                        secondary={
                                            message.createdAt?.$timestamp?.i
                                                ? format(
                                                      new Date(
                                                          parseInt(
                                                              message.createdAt
                                                                  ?.$timestamp
                                                                  ?.i
                                                          )
                                                      ),
                                                      "'Delivered on' eeee 'at' p"
                                                  )
                                                : "Sending..."
                                        }
                                    ></ListItemText>
                                </Grid>
                            </Grid>
                        </ListItem>
                    ))}
            </List>
            <form className="c-message-form" onSubmit={submitMessage}>
                <Grid container style={{ padding: "20px" }}>
                    <Grid item xs={9} sm={11}>
                        <TextField
                            id="outlined-basic-email"
                            name="text"
                            label="Type Something"
                            fullWidth
                            required
                        />
                        <input
                            type="hidden"
                            name="username"
                            value={currentUsername}
                        />
                    </Grid>
                    <Grid
                        item
                        xs={3}
                        sm={1}
                        align="right"
                        style={{ paddingLeft: "10px" }}
                    >
                        <Fab
                            color="primary"
                            aria-label="add"
                            type="submit"
                            style={{
                                backgroundColor: "#1976d2",
                            }}
                        >
                            <SendIcon />
                        </Fab>
                    </Grid>
                </Grid>
            </form>
        </>
    );
}
