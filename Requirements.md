# Product development steps:

1. Requirements gathering
2. Architecture design
3. Ticket creation & prioritization
4. Build!

## Requirements

-   Messaging
    -   MVP
        -   DMs only
        -   Text
        -   Images
        -   Emoji Picker
    -   Polished
        -   Channels and multi-user DMs
        -   Videos
-   User authentication
    -   MVP
        -   Sign up
        -   Sign in
        -   Sign out
        -   Forgot password via email
        -   User search
    -   Polished
        -   CAPTCHA
        -   File Sharing
-   App Features
    -   MVP
        -   Push notifications
        -   PWA
-   Calling
    -   Polished
        -   Video
        -   Audio
        -   Peer to peer

## Architecture

-   Entities
    -   User
        -   Fields
            -   credential fields
            -   username
            -   profileImage
    -   Message
        -   Fields
            -   username
            -   textBody
            -   attachment
            -   createdAt
-   Technologies
    -   Framework, CI/CD, web hosting: Next.js/vercel
    -   Frontend: React, Material UI
    -   Backend: Next.js Functions, Websockets
    -   DB: Firestore
    -   Authentication: ...?
    -   Calling: WebRTC
    -   Other: ESLint, Prettier, GitLab
